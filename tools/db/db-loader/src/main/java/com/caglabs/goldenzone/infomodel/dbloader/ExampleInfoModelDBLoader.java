package com.caglabs.goldenzone.infomodel.dbloader;

import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUserDao;

import java.util.ArrayList;
import java.util.List;

public class ExampleInfoModelDBLoader extends AbstractDBLoaderBase {
    private List<GoldenzoneUser> users = new ArrayList<GoldenzoneUser>();
    private int numUsers = -1;

    public void loadDB() {
        final GoldenzoneUserDao goldenzoneUserDao = new GoldenzoneUserDao();
        injectEntityManager(goldenzoneUserDao);
        withinTransaction(new Runnable() {
            @Override
            public void run() {
                if (numUsers == -1) {
                    numUsers = 3;
                }
                for (int i = 0; i < numUsers; ++i) {
                    users.add(new GoldenzoneUser("kalle" + i, "Kalle #" + i));
                }
                for (GoldenzoneUser user : users) {
                    goldenzoneUserDao.save(user);
                }
            }
        });
    }

    public ExampleInfoModelDBLoader withNumUsers(int numUsers) {
        this.numUsers = numUsers;
        return this;
    }

    public ExampleInfoModelDBLoader withUser(GoldenzoneUser user) {
        users.add(user);
        return this;
    }

    public static void main(String[] args) throws DBLoadException {
        new ExampleInfoModelDBLoader().execute();
    }
}
