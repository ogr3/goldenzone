package com.caglabs.goldenzone.helloservice.impl;

import com.caglabs.goldenzone.helloservice.client.Hello;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUserDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Simple test bean.
 */
@WebService
@Stateless
@Remote(Hello.class)
public class HelloBean implements Hello {
    private Logger logger = Logger.getLogger(HelloBean.class.getName());

    @Inject
    private GoldenzoneUserDao goldenzoneUserDao;

    @PersistenceContext(unitName = "GoldenzonePU")
    private EntityManager entityManager;

    @Override
    public String sayHello(String name) throws HelloException {
        if (goldenzoneUserDao.entityManager == null) {
            goldenzoneUserDao.entityManager = entityManager;
        }
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(name);
        if (user != null) {
            String message = "Hello " + user.getDisplayName() + " (id=" + user.getId() + ")!!! The time is " +
                    new SimpleDateFormat("HH:mm:ss").format(new Date());
            logger.info(message);
            return message;
        } else {
            String message = "Unknown user: " + name;
            logger.info(message);
            throw new HelloException(message);
        }
    }

    @Override
    public void addUser(String username, String displayName) throws HelloException {
        if (goldenzoneUserDao.entityManager == null) {
            goldenzoneUserDao.entityManager = entityManager;
        }
        goldenzoneUserDao.save(new GoldenzoneUser(username, displayName));
    }

    @Override
    public void removeUser(String username) throws HelloException {
        if (goldenzoneUserDao.entityManager == null) {
            goldenzoneUserDao.entityManager = entityManager;
        }
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(username);
        if (user == null) {
            String message = "removeUser failed, username does not exist: " + username;
            logger.info(message);
            throw new HelloException(message);
        }
        goldenzoneUserDao.remove(user);
    }

    @Override
    public void updateUser(String username, String displayName) throws HelloException {
        if (goldenzoneUserDao.entityManager == null) {
            goldenzoneUserDao.entityManager = entityManager;
        }
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(username);
        if (user == null) {
            String message = "updateUser failed, username does not exist: " + username;
            logger.info(message);
            throw new HelloException(message);
        }
        user.setDisplayName(displayName);
        entityManager.merge(user);
    }
}
